//
//  Document.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import Cocoa
import VerveCalc

class Document: NSDocument {
    // own instance
    public var loadData: VerveSaveData?

    // object owned by another object (weak)
    private weak var viewController: ViewController?
    private weak var windowController: WindowController?

    override init() {
        super.init()
    }

    override class var autosavesInPlace: Bool {
        return true
    }

    override func makeWindowControllers() {
        // Returns the Storyboard that contains your Document window.
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        windowController = storyboard.instantiateController(withIdentifier: "Document Window Controller") as? WindowController
        self.addWindowController(windowController!)
        viewController = windowController!.contentViewController as? ViewController
    }

    override func data(ofType typeName: String) throws -> Data {
        return NSKeyedArchiver.archivedData(withRootObject: viewController!.createSaveData)
    }

    override func read(from data: Data, ofType typeName: String) throws {
        loadData = NSKeyedUnarchiver.unarchiveObject(with: data) as? VerveSaveData
    }


}

