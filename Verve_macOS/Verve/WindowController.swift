//
//  WindowController.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import Cocoa
import VerveCalc

class WindowController: NSWindowController {
    @IBOutlet weak var typeSelector: NSSegmentedCell!
    @IBOutlet weak var modeSelector: NSSegmentedCell!
    @IBOutlet weak var electronCurrentValue: NSToolbarItem!
    @IBOutlet weak var plusMinus: NSSegmentedCell!
    @IBOutlet weak var slider: NSSlider!
    @IBOutlet weak var efButton: NSButton!
    @IBOutlet weak var mfButton: NSButton!
    @IBOutlet weak var eflButton: NSButton!
    @IBOutlet weak var eqlButton: NSButton!
    @IBOutlet weak var emfButton: NSButton!
    @IBOutlet weak var cfButton: NSButton!
    
    // computed property
    public var getValue: Int { return (plusMinus.selectedSegment == 1 ? -1 : 1) * slider.integerValue }
    public var editType: EditType {
        return typeSelector.selectedSegment == 0 ? EditType.electron : EditType.slCurrent
    }
    public var editMode: EditMode {
        switch modeSelector.selectedSegment {
        case 0:
            return EditMode.addMode
        case 1:
            return EditMode.editMode
        case 2:
            return EditMode.fixMode
        case 3:
            return EditMode.deleteMode
        default:
            return EditMode.disableMode
        }
    }
    public var efView: Bool {
        set { efButton.state = newValue ? .on : .off }
        get { return efButton.state == .on }
    }
    public var mfView: Bool {
        set { mfButton.state = newValue ? .on : .off }
        get { return mfButton.state == .on }
    }
    public var eflView: Bool {
        set { eflButton.state = newValue ? .on : .off }
        get { return eflButton.state == .on }
    }
    public var eqlView: Bool {
        set { eqlButton.state = newValue ? .on : .off }
        get { return eqlButton.state == .on }
    }
    public var emfView: Bool {
        set { emfButton.state = newValue ? .on : .off }
        get { return emfButton.state == .on }
    }
    public var cfView: Bool {
        set { cfButton.state = newValue ? .on : .off }
        get { return cfButton.state == .on }
    }
    public var buttonData: (Bool, Bool, Bool, Bool, Bool, Bool) {
        set {
            (efView, mfView, eflView, eqlView, emfView, cfView) = newValue
        }
        get { return (efView, mfView, eflView, eqlView, emfView, cfView) }
    }
    public var viewController: ViewController? { return contentViewController as! ViewController? }

    override func windowDidLoad() {
        super.windowDidLoad()
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        let sheetViewController = segue.destinationController as! SheetViewController
        sheetViewController.representedObject = viewController
    }
    
    public func displayValue() {
        electronCurrentValue.label = "\(getValue)[\(editType == EditType.electron ? "C" : "A")]"
    }

    @IBAction func changeValue(_ sender: Any) {
        displayValue()
        viewController?.changeValue(getValue)
    }

    @IBAction func changeEF(_ sender: Any) {
        if sender is NSMenuItem {
            efButton.setNextState()
        }
        if efButton.state == .on {
            mfButton.state = .off
        }
        viewController!.draw = true
    }

    @IBAction func changeMF(_ sender: Any) {
        if sender is NSMenuItem {
            mfButton.setNextState()
        }
        if mfButton.state == .on {
            efButton.state = .off
        }
        viewController!.draw = true
    }

    @IBAction func changeEFL(_ sender: Any) {
        if sender is NSMenuItem {
            eflButton.setNextState()
        }
        viewController!.draw = true
    }

    @IBAction func changeEQL(_ sender: Any) {
        if sender is NSMenuItem {
            eqlButton.setNextState()
        }
        viewController!.draw = true
    }
    @IBAction func changeEMF(_ sender: Any) {
        if sender is NSMenuItem {
            emfButton.setNextState()
        }
        if !emfView {
            viewController!.cancelAllForce()
        }
        viewController!.draw = true
    }

    @IBAction func changeCF(_ sender: Any) {
        if sender is NSMenuItem {
            cfButton.setNextState()
        }
        viewController!.draw = true
    }

    public func changeType(_ type: EditType) {
        typeSelector.selectSegment(withTag: type.rawValue)
        displayValue()
    }

    @IBAction func changeTypeByButton(_ sender: Any) {
        changeMode(EditMode.addMode)
    }

    private func changeValueEnabled(_ flag: Bool) {
        plusMinus.isEnabled = flag
        slider.isEnabled = flag
    }
    
    @IBAction func changeModeByButton(_ sender: Any) {
        switch modeSelector.selectedSegment {
        case EditMode.addMode.rawValue:
            viewController?.unsettledAll()
            changeValueEnabled(true)
        case EditMode.editMode.rawValue:
            changeValueEnabled(true)
            break
        case EditMode.fixMode.rawValue:
            viewController?.unsettledAll()
            changeValueEnabled(false)
        default:
            viewController?.unsettledAll()
            changeValueEnabled(false)
            break
        }
        displayValue()
    }
    
    public func changeMode(_ mode: EditMode) {
        modeSelector.selectSegment(withTag: mode.rawValue)
        changeModeByButton(modeSelector)
    }
    
    public func setEditValue(_ value: Double) {
        let iv = Int(value)
        let sign = iv.signum()
        slider.integerValue = iv * sign
        plusMinus.selectSegment(withTag: sign)
        displayValue()
    }

}
