//
//  Document.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import UIKit
import VerveCalc

class Document: UIDocument {
    // own instance
    public var loadData: VerveSaveData?
    public weak var viewController: DocumentViewController?
    
    override func contents(forType typeName: String) throws -> Any {
        guard let vc = viewController else { return Data() }
        return NSKeyedArchiver.archivedData(withRootObject: vc.createSaveData)
    }
    
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        guard let data = contents as? Data else { return }
        guard let savedData = NSKeyedUnarchiver.unarchiveObject(with: data) as? VerveSaveData else { return }
        loadData = savedData
    }
}
