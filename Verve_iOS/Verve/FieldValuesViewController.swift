//
//  FieldValuesViewController.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/10.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import UIKit
import VerveCalc

class FieldValuesViewController: UIViewController {
    @IBOutlet weak var xEfSlider: UISlider!
    @IBOutlet weak var yEfSlider: UISlider!
    @IBOutlet weak var zEfSlider: UISlider!
    @IBOutlet weak var efEnableSwitch: UISwitch!
    @IBOutlet weak var xMfSlider: UISlider!
    @IBOutlet weak var yMfSlider: UISlider!
    @IBOutlet weak var zMfSlider: UISlider!
    @IBOutlet weak var mfEnableSwitch: UISwitch!

    // object owned by another object
    public weak var documentViewController: DocumentViewController?
    public weak var verveCalculator2D: VerveCalculator2D?
    
    // computed property
    open var biasElectricField: Point3DwithBool { return Point3DwithBool(value: Point3D(x: Double(xEfSlider.value)*1000, y: Double(yEfSlider.value)*1000, z: Double(zEfSlider.value)*1000), enable: efEnableSwitch.isOn) }
    open var biasMagneticField: Point3DwithBool { return Point3DwithBool(value: Point3D(x: Double(xMfSlider.value), y: Double(yMfSlider.value), z: Double(zMfSlider.value)), enable: mfEnableSwitch.isOn) }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // ViewControllerがメモリにロードされた後に呼び出される
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let bef = verveCalculator2D!.biasElectricField
        xEfSlider.value = Float(bef.value.x / 1000)
        yEfSlider.value = Float(bef.value.y / 1000)
        zEfSlider.value = Float(bef.value.z / 1000)
        efEnableSwitch.isOn = bef.enable
        let bmf = verveCalculator2D!.biasMagneticField
        xMfSlider.value = Float(bmf.value.x)
        yMfSlider.value = Float(bmf.value.y)
        zMfSlider.value = Float(bmf.value.z)
        mfEnableSwitch.isOn = bmf.enable
    }
    
    @IBAction func changeEfSlider(_ sender: AnyObject) {
        if efEnableSwitch.isOn {
            verveCalculator2D!.biasElectricField = biasElectricField
            documentViewController?.drawLines()
        }
    }
    @IBAction func changeMfSlider(_ sender: AnyObject) {
        if mfEnableSwitch.isOn {
            verveCalculator2D!.biasMagneticField = biasMagneticField
            documentViewController?.drawLines()
        }
    }
    
    @IBAction func resetElectricField(_ sender: AnyObject) {
        xEfSlider.value = 0.0
        yEfSlider.value = 0.0
        zEfSlider.value = 0.0
        changeEfEnable(xEfSlider)
    }
    @IBAction func resetMagneticField(_ sender: AnyObject) {
        xMfSlider.value = 0.0
        yMfSlider.value = 0.0
        zMfSlider.value = 0.0
        changeMfEnable(xMfSlider)
    }
    
    @IBAction func changeEfEnable(_ sender: AnyObject) {
        let bef = efEnableSwitch.isOn ? biasElectricField : Point3DwithBool()
        verveCalculator2D!.biasElectricField = bef
        documentViewController!.drawLines()
    }
    
    @IBAction func changeMfEnable(_ sender: AnyObject) {
        let bmf = mfEnableSwitch.isOn ? biasMagneticField : Point3DwithBool()
        verveCalculator2D!.biasMagneticField = bmf
        documentViewController!.drawLines()
    }


}
