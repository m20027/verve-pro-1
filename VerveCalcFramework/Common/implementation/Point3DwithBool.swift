//
//  Point3DwithBool.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/09.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public struct Point3DwithBool {
    // owned instance
    public var value: Point3D
    public var enable: Bool
    
    public init(value v: Point3D = Point3D(), enable e: Bool = true) {
        value = v
        enable = e
    }

}







