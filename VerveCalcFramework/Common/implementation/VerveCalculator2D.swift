//
//  VerveCalculator2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public class VerveCalculator2D: NSObject, Sequence  {
    // static constant
    public static let mx = 1024
    public static let my = 698

    // owned instance
    public var biasElectricField: Point3DwithBool = Point3DwithBool()
    public var biasMagneticField: Point3DwithBool = Point3DwithBool()

    // owned instance (private set)
    public private(set) var nCalc: Int = 1
    public private(set) var dr: Double = 1
    public private(set) var step: Int = 1
    public private(set) var arrowStep: Int = 1

    public private(set) var nElectron: Int = 0
    public private(set) var electrons: [Electron2D] = [Electron2D]()
    public private(set) var nSLCurrent: Int = 0
    public private(set) var width = 0
    public private(set) var height = 0
    public private(set) var skip = 0
    public private(set) var slCurrents: [SLCurrent2D] = [SLCurrent2D]()
    public private(set) var exfields: [Double] = [Double]()
    public private(set) var eyfields: [Double] = [Double]()
    public private(set) var eafields: [Double] = [Double]()
    public private(set) var tmp1fields: [Double] = [Double]()
    public private(set) var tmp2fields: [Double] = [Double]()
    //    private let u0: Double = 4.0e-7 * M_PI

    // private instance
    fileprivate var templateExfields: [Double] = [Double]()
    fileprivate var templateEyfields: [Double] = [Double]()
    fileprivate var recalc: Bool = false

    // computed property
    public var smoothNplot: Int { return smoothNCalc / smoothStep + 1 }
    private var calcArrowStep: Int { return Int(200.0 / dr / Double(step)) }

    // private constant
    private let e0: Double = 8.85418782e-12
    private let smoothDR : Double = 0.2
    private let smoothStep : Int = 60
    private let smoothNCalc : Int = 6000
    private let roughDR : Double = 35.0
    private let roughStep : Int = 1
    private let roughNCalc : Int = 50

    public convenience init(nCalc: Int, dr: Double, step: Int) {
        self.init()
        self.nCalc = nCalc
        self.dr = dr
        self.step = step
        arrowStep = calcArrowStep
    }

    public func createTemplate() {
        width = VerveCalculator2D.mx
        height = VerveCalculator2D.my
        skip = width * 2 + 1
        let length = width * height
        exfields = [Double](repeating: 0.0, count: length)
        eyfields = [Double](repeating: 0.0, count: length)
        eafields = [Double](repeating: 0.0, count: length)
        tmp1fields = [Double](repeating: 0.0, count: length)
        tmp2fields = [Double](repeating: 0.0, count: length)
        let length2 = skip * (height * 2 + 1)
        templateExfields = [Double](repeating: 0.0, count: length2)
        templateEyfields = [Double](repeating: 0.0, count: length2)
        var tmp1 = [Double](repeating: 0.0, count: length2)
        var tmp2 = [Double](repeating: 0.0, count: length2)
        var i: Int = 0
        for y in (-height ... height) {
            for x in (-width ... width) {
                templateExfields[i] = Double(x)
                templateEyfields[i] = Double(y)
                i += 1
            }
        }
        setDist(&tmp1, x: templateExfields, y: templateEyfields)
        setMul(&tmp2, x: tmp1, y: tmp1)
        tmp1 *= tmp2
        tmp1[skip * height + width] = 1.0
        setDiv(&tmp2, const: 1.0 / (4.0 * Double.pi * e0), x: tmp1)
        templateExfields *= tmp2
        templateEyfields *= tmp2
    }

    public func updateEfields() {
        exfields <- biasElectricField.value.x
        eyfields <- biasElectricField.value.y
        electrons.forEach { e in
            let ex = Int(e.x)
            let ey = Int(e.y)
            if ex >= 0 && ex < VerveCalculator2D.mx && ey >= 0 && ey < VerveCalculator2D.my {
                let startPos = (height - ey) * skip + (width - ex)
                setMmov(templateExfields, offset: startPos, to: &tmp1fields, m: width, n: height, ta: skip, tc: width)
                setMmov(templateEyfields, offset: startPos, to: &tmp2fields, m: width, n: height, ta: skip, tc: width)
                setVsma(tmp1fields, const: e.charge, add: exfields, to: &exfields)
                setVsma(tmp2fields, const: e.charge, add: eyfields, to: &eyfields)
            }
        }
        setDist(&eafields, x: exfields, y: eyfields)
        sign(eafields, to: &tmp2fields)
    }

    public func makeIterator() -> AnyIterator<Electron2D> {
        var index : Int = 0
        return AnyIterator {
            if index < self.nElectron {
                let ans = self.electrons[index]
                index += 1
                return ans
            } else {
                return .none
            }
        }
    }

    public func addElectron(_ electron: Electron2D){
        electrons.append(electron)
        nElectron += 1
        updateEfields()
    }

    public func removeElectron(_ electron: Electron2D) -> Int? {
        func indexOfElectron() -> Int? {
            var index: Int = 0
            for e in self.electrons {
                if e.isEqual(electron) {
                    return index
                }
                index += 1
            }
            return nil
        }

        if let index = indexOfElectron() {
            electrons.remove(at: index)
            nElectron -= 1
            updateEfields()
            return index
        } else {
            return nil
        }
    }

    /* 簡易計算(整数座標によるテーブル引き) */
    public func calcExy(_ position: Point2D) -> Point2D {
        let py = Int(position.y.rounded())
        let px = Int(position.x.rounded())
        if px >= 0 && py >= 0 && px < width && py < height {
            let p = py * width + px
            return Point2D(x: exfields[p], y: eyfields[p]);
        } else {
            return calcExyDetail(position)
        }
    }

    /* 高速計算(電気力線の初期位置のみ小数点で計算) */
    public func calcExyDetail(_ position: Point2D) -> Point2D {
        let ep = Point2D(x: biasElectricField.value.x, y: biasElectricField.value.y)
        let ps = electrons.map { $0.position }
        let cs = electrons.map { $0.charge }
        let xds = position.x - ps.map { $0.x }
        let yds = position.y - ps.map { $0.y }
        let rs = distX(xds, y: yds)
        let rs3 = rs * rs * rs
        let cc = 4.0 * Double.pi * e0
        var c = cs / rs3 / cc
        removeInf(&c, ref: rs3)
        return ep + Point2D(x: sum(c * xds), y: sum(c * yds))
    }

//    // removeInf の導入により削除
//    //    public func calcExyWithoutAnElectron(position: Point2D, electron: Electron2D) -> Point2D {
//    //        var ep = Point2D(x: _biasElectricField.x, y: _biasElectricField.y)
//    //        _electrons.forEach { (e) in
//    //            if !(e == electron) {
//    //                e.addExy(electron.position, e: &ep)
//    //            }
//    //        }
//    //        return ep
//    //    }

    public func calcExyByPxs(_ pxs: [Double], pys: [Double]) -> (ex: [Double], ey: [Double], eabs: [Double]) {
        let poss = pys * Double(width) + pxs
        var exs = [Double](repeating: 0.0, count: pxs.count)
        var eys = [Double](repeating: 0.0, count: pys.count)
        var eas = [Double](repeating: 0.0, count: pys.count)
        for (i, pos) in poss.enumerated() {
            let p = Int(pos)
            exs[i] = exfields[p]
            eys[i] = eyfields[p]
            eas[i] = eafields[p]
        }
        return (exs, eys, eas)
    }

    /* 高速計算(テンプレートで計算したテーブルを引くことにしたのでもう使わない) */
    //    public func calcExyByPxs(pxs: [Double], pys: [Double]) -> (ex: [Double], ey: [Double], eabs: [Double]) {
    //        var exs = [Double](count: pxs.count, repeatedValue: _biasElectricField.x)
    //        var eys = [Double](count: pxs.count, repeatedValue: _biasElectricField.y)
    //        let cc = 4.0 * M_PI * e0
    //        _electrons.forEach { e in
    //            let rxs = pxs - e.x
    //            let rys = pys - e.y
    //            let rs = distX(rxs, y: rys)
    //            let c = e.charge / cc / rs / rs / rs
    //            exs += c * rxs
    //            eys += c * rys
    //        }
    //        return (exs, eys, distX(exs, y: eys))
    //    }
    

    public func calcNewPosition(_ electron: Electron2D, ef: Point2D, position: inout Point2D){
        let c = electron.chargeSign / ef.r * dr
        position.addOther(ef, multiply: c)
    }

    public func willContinueCalc(_ eabs:Double, line: Line2D, pass: Int, position: Point2D) -> Bool {
        if eabs < 1e-9 {
            return false
        }
        for e in self.electrons {
            let diff = e.position.diff(position)
            if diff.r < 1.0 {
                if pass == 0 {
                    e.appendInputs(theta: diff.angle, recalc: &recalc)
                    line.reachElectron = true
                }
                return false
            }
        }
        return true
    }

    public func calcAnELine(_ electron: Electron2D, line: Line2D, pass: Int, rotateStartAngle: Double = 0.0) {
        var calcPosition = Point2D()
        line.clear(electron.position, rotateStartAngle: rotateStartAngle)
        for p in line {
            calcPosition.setValueX(p.x, y: p.y)
            break
        }
        for i in 0 ..< self.nCalc {
            let ef = i < 20 ? self.calcExyDetail(calcPosition) : self.calcExy(calcPosition) // 近傍だけは精密計算(残りは近似)
            self.calcNewPosition(electron, ef: ef, position: &calcPosition)
            if i % step == 0 {
                line.addPoint(calcPosition)
            }
            if !self.willContinueCalc(ef.r, line: line, pass: pass, position: calcPosition) {
                break
            }
        }
    }

    public func calcELines() {
        electrons.forEach { (e) in
            e.clearRotateAngle()
        }
        recalc = false
        for pass in 0 ..< 2 {
            electrons.forEach { e in
                e.clearUsed()
            }
            electrons.forEach { e in
                let ec = e.charge
                let iCharge = Int(ec)
                let ef = calcExyDetail(e.position)
                let rotateAngle = e.rotateStartAngle.isNaN ?
                    atan2(ef.y, ef.x)
                        + (iCharge % 2 == 0 ? Double.pi / ec : 0.0)
                        + (ec > 0 ? 0.0 : Double.pi) : e.rotateStartAngle
                e.forEach { (l) in
                    if l.fixedTheta.isNaN {
                        calcAnELine(e, line: l, pass: pass, rotateStartAngle: rotateAngle)
                    } else {
                        l.clear(e.position, rotateStartAngle: rotateAngle)
                    }
                    l.used = true
                }
            }
            if !recalc {
                break
            }
        }
    }

    public func addSLCurrent(_ slCurrent: SLCurrent2D){
        slCurrents.append(slCurrent)
        nSLCurrent += 1
    }

    public func removeSLCurrent(_ slCurrent: SLCurrent2D) -> Int? {
        if let index = slCurrents.index(of: slCurrent){
            nSLCurrent -= 1
            slCurrents.remove(at: index)
            return index
        }else {
            return nil
        }
    }

    /* 通常計算 */
    //    public func calcHxy(_ position: Point2D) -> Point2D {
    //        var hp = Point2D(x: biasMagneticField.x, y: biasMagneticField.y)
    //        slCurrents.forEach { (c) in
    //            let d = position.diff(c.position)
    //            let r = d.r
    //            if r > 1e-9 {
    //                let a = c.current / 2.0 / Double.pi / r / r
    //                hp.addValueX(-a * d.y, y: a * d.x)
    //            }
    //        }
    //        return hp
    //    }

    /* 高速計算 */
    public func calcHxy(_ position: Point2D) -> Point2D {
        let hp = Point2D(x: biasMagneticField.value.x, y: biasMagneticField.value.y)
        let ps = slCurrents.map { $0.position }
        let cs = slCurrents.map { $0.current }
        let xds = position.x - ps.map { $0.x }
        let yds = position.y - ps.map { $0.y }
        let rs = distX(xds, y: yds)
        let rs2 = rs * rs
        let cc = 2.0 * Double.pi
        var c = cs / rs2 / cc
        removeInf(&c, ref: rs2)
        return hp + Point2D(x: -sum(c * yds), y: sum(c * xds))
    }

    // 電位描画、電位差描画は本バージョンではとりあえず除去(要望があれば追加)
    //    public func potential(_ position: Point2D) -> Double {
    //        var v = 0.0
    //        electrons.forEach { (e) in
    //            let d = position.diff(e.position)
    //            if d.r > 1e-9 {
    //                v += e.charge / 4.0 / Double.pi / e0 / d.r
    //            }
    //        }
    //        return v
    //    }
    //
    //    public func potentialBetween2Points(_ p1: Point2D, and p2: Point2D) -> Double {
    //        return fabs(potential(p1) - potential(p2))
    //    }

    public func setSmoothParameter() {
        dr = smoothDR
        nCalc = smoothNCalc
        step = smoothStep
        arrowStep = calcArrowStep
    }

    public func setRoughParameter() {
        dr = roughDR
        nCalc = roughNCalc
        step = roughStep
        arrowStep = calcArrowStep
    }

    public func force(_ electron: Electron2D, vx: Double, vy: Double) -> [Double] {
        //        let exy = calcExyWithoutAnElectron(electron.position, electron: electron)
        let exy = calcExyDetail(electron.position)
        let c = electron.charge
        var f = [exy.x, exy.y] * c
        let cmz = c * biasMagneticField.value.z
        f += [cmz * vy, -cmz * vx]
        return f
    }

    public func calcHxyByPxs(_ pxs: [Double], pys: [Double]) -> (hx: [Double], hy: [Double], habs: [Double]) {
        var hxs = [Double](repeating: biasMagneticField.value.x, count: pxs.count)
        var hys = [Double](repeating: biasMagneticField.value.y, count: pxs.count)
        let cc = 2.0 * Double.pi
        slCurrents.forEach { s in
            let rxs = pxs - s.x
            let rys = pys - s.y
            let rs = distX(rxs, y: rys)
            let c = s.current / cc /  (rs * rs)
            hxs -= c * rys
            hys += c * rxs
        }
        return (hxs, hys, distX(hxs, y: hys))
    }

    public func createSaveData(version v: Int, capture c: Data, buttons: (Bool, Bool, Bool, Bool, Bool, Bool)) -> VerveSaveData {
        return VerveSaveData(version: v, capture: c, date: Date(), title: "No title", buttons: buttons, verveCalculator2D: self)
    }
}
