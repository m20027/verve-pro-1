//
//  Verve2DScene.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import SpriteKit

public class Verve2DScene: SKScene {
    enum ELNode: String {
        case CoulombForceParentNode = "CoulombForceParentNode"
        case ElectronParentNode = "ElectronParentNode"
        case SLCurrentParentNode = "SLCurrentParentNode"
        case LineParentNode = "LineParentNode"
        case FieldExhibitNode = "FieldExhibitNode"
        //        case PotentialLabelNode = "PotentialLabelNode"
        //        case PotentialLineNode = "PotentialLineNode"
        //        case ElectricFieldStrengthLabelNode = "ElectricFieldStrengthLabelNode"
        case EquipotentialNode = "EquipotentialNode"
    }

    open var coulombForceParentNode: SKNode? { return childNodeWithELNode(.CoulombForceParentNode) }
    open var electronParentNode: SKNode? { return childNodeWithELNode(.ElectronParentNode) }
    open var slCurrentParentNode: SKNode? { return childNodeWithELNode(.SLCurrentParentNode) }
    open var equipotentialNode: EquipotentialNode2D? { return childNodeWithELNode(.EquipotentialNode) as? EquipotentialNode2D }
    open var electronNodes: [ElectronNode2D] { return electronParentNode!.children as! [ElectronNode2D] }
    open var lineParentNode: LineParentNode? { return childNodeWithELNode(.LineParentNode) as? LineParentNode }
    open var fieldExhibitNode: FieldExhibitNode2D? { return childNodeWithELNode(.FieldExhibitNode) as? FieldExhibitNode2D }
    //
    open func addElectronNode(_ electronNode: ElectronNode2D) {
        electronParentNode!.addChild(electronNode)
        lineParentNode!.addLinesWithElectron(electronNode.electron!)
        coulombForceParentNode!.addChild(electronNode.coulombForceNode!)
    }

    open func addSLCurrentNode(_ slCurrentNode: SLCurrentNode2D) {
        slCurrentParentNode!.addChild(slCurrentNode)
    }
    
    open func removeElectronNode(_ electronNode: ElectronNode2D) {
        electronNode.coulombForceNode!.removeFromParent()
        lineParentNode!.deleteLinesWithElectron(electronNode.electron!)
        electronNode.removeFromParent()
    }

    open func removeSLCurrentNode(_ slCurrentNode: SLCurrentNode2D) {
        slCurrentNode.removeFromParent()
    }
    
    //    override open func mouseDown(with theEvent: NSEvent) {
    //        nextResponder?.mouseDown(with: theEvent)
    //    }
    //
    override open func didMove(to view: SKView) {
        // サイズは調整しない
        //        self.size = view.bounds.size

        self.backgroundColor = SKColor(red: 18.0 / 255.0, green: 6.0 / 255.0, blue: 45.0 / 255.0, alpha: 1.0)
        // let screenSize = view.bounds.size
        let screenSize = self.size
        [
            ELNode.EquipotentialNode: EquipotentialNode2D(screenSize: screenSize)
            ].forEach { (n, o) in
                o.name = n.rawValue
                addChild(o)
        }
        
        [ ELNode.CoulombForceParentNode, ELNode.ElectronParentNode, ELNode.SLCurrentParentNode  ].forEach { (nn) in
            let tmp = SKNode()
            tmp.name = nn.rawValue
            addChild(tmp)
        }
        [
            ELNode.LineParentNode: LineParentNode(),
            ELNode.FieldExhibitNode: FieldExhibitNode2D(screenSize: screenSize, arrowDistance: 48.0),
            ].forEach { (n, o) in
                o.name = n.rawValue
                addChild(o)
        }
        //
        //        //電位ポテンシャルを表すラベル
        //        let potentialLabeNode = SKLabelNode()
        //        potentialLabeNode.fontSize = 40
        //        potentialLabeNode.fontName = "HiraKakuProN-W3"
        //        potentialLabeNode.fontColor = SKColor.yellow
        //        potentialLabeNode.text = ""
        //        potentialLabeNode.verticalAlignmentMode = .bottom
        //        potentialLabeNode.horizontalAlignmentMode = .center
        //        potentialLabeNode.zPosition = 3
        //        potentialLabeNode.isHidden = true
        //        potentialLabeNode.name = ELNode.PotentialLabelNode.rawValue
        //        addChild(potentialLabeNode)
        //
        //        let potentialLineNode = SKShapeNode()
        //        potentialLineNode.isHidden = true
        //        potentialLineNode.strokeColor = SKColor.yellow
        //        potentialLineNode.name = ELNode.PotentialLineNode.rawValue
        //        potentialLineNode.zPosition = 3
        //        potentialLineNode.lineWidth = 3
        //        self.addChild(potentialLineNode)
        //
        //
        //        //このノードで電界の強度を表示する
        //        let electricFieldStrengthLabelNode = SKLabelNode()
        //        electricFieldStrengthLabelNode.fontSize = 40
        //        electricFieldStrengthLabelNode.fontName = "HiraKakuProN-W3"
        //        electricFieldStrengthLabelNode.zPosition = 3
        //        electricFieldStrengthLabelNode.fontColor = SKColor.yellow
        //        electricFieldStrengthLabelNode.verticalAlignmentMode = .center
        //        electricFieldStrengthLabelNode.horizontalAlignmentMode = .left
        //        electricFieldStrengthLabelNode.name = ELNode.ElectricFieldStrengthLabelNode.rawValue
        //        electricFieldStrengthLabelNode.isHidden = true
        //        self.addChild(electricFieldStrengthLabelNode)
        //
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.scaleMode = .fill
    }

    public func cancelAllForce() {
        electronNodes.forEach { node in
            node.physicsBody!.velocity.dx = 0.0
            node.physicsBody!.velocity.dy = 0.0
        }
    }
        
    //#### local functions
    private func childNodeWithELNode(_ elNode: ELNode) -> SKNode? {
        return childNode(withName: elNode.rawValue)
    }

    //    deinit{
    //        print("2DScene deinit")
    //    }
}
