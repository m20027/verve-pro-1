//
//  ElectronNode2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import SpriteKit

public class ElectronNode2D: SKShapeNode {
    // owned instance (private set)
    open private(set) var electron: Electron2D?
    open private(set) var coulombForceNode: CoulombForceNode2D?
    
    // owned instance (didSet)
    override open var position : CGPoint {
        didSet {
            electron?.position = Point2D(cgpoint: position)
            coulombForceNode?.position = position
        }
    }

    //  owned instance(private)
    private var selected: Bool = false
    
    public override init() {
        super.init()
    }
    
    public convenience init(electron: Electron2D){
        self.init()
        self.electron = electron
        setNodeRadiusAndColor(electron.charge)
        self.coulombForceNode = CoulombForceNode2D(electron: electron)
        self.position = CGPoint(x: CGFloat(electron.x), y: CGFloat(electron.y))
        self.name = "ElectronNode2D"
        self.zPosition = 2
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setSelected() {
        selected = true
        setNodeRadiusAndColor(electron!.charge)
    }
    
    public func unsetSelected() {
        selected = false
        setNodeRadiusAndColor(electron!.charge)
    }

    open func setNodeRadiusAndColor(_ chargeValue: Double){
        electron?.charge = chargeValue
        let absChargeValue = abs(CGFloat(chargeValue))
        let radius = (2.0*absChargeValue+10.0)*1.5
        let color = chargeValue < 0 ?
            SKColor(red: 0, green: 0.2, blue: absChargeValue*3.0/100.0+0.7, alpha: 1) :
            SKColor(red: absChargeValue*3.0/100.0+0.7, green: 0.2, blue: 0, alpha: 1)
        let path = CGMutablePath()
        path.move(to: CGPoint(x: -radius, y: 0))
        path.addArc(center: CGPoint(x: 0, y: 0), radius: radius, startAngle: CGFloat(Double.pi), endAngle: CGFloat(-Double.pi), clockwise: true)
        //        CGPathMoveToPoint(path, nil, -radius, 0)
        //        CGPathAddArc(path, nil, 0, 0, radius, CGFloat(M_PI), CGFloat(-M_PI), true)
        self.path = path
        self.fillColor = color
        self.strokeColor = electron!.enableForceAction ? SKColor(red: 0.576, green: 0.878, blue: 0.741, alpha: 1.0) : SKColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 1.0)
        self.lineWidth = selected ? 4 : 1
        self.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.linearDamping = 0.0
        self.physicsBody?.restitution = 0.0

        self.physicsBody?.isDynamic = electron!.enableForceAction
    }

    open func updateElectronPosition(_ redraw: inout Bool) {
        if Int(position.x) != Int(electron!.x) || Int(position.y) != Int(electron!.y) {
            electron?.position = Point2D(cgpoint: position)
            redraw = true
        }
    }
    
    //    open func resetElectronPosition() {
    //        if Int(position.x) != Int(electron!.x) || Int(position.y) != Int(electron!.y) {
    //            position = CGPoint(x: electron!.x, y: electron!.y)
    //            coulombForceNode!.position = position
    //        }
    //    }

}
