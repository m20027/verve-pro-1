//
//  VerveEnums.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public enum EditMode: Int {
    case addMode = 0
    case editMode = 1
    case fixMode = 2
    case deleteMode = 3
    case disableMode = 4

    mutating func setAddMode() { self = .addMode }
    mutating func setEditMode() { self = .editMode }
    mutating func setFixMode() { self = .fixMode }
    mutating func setDeleteMode() { self = .deleteMode }
    mutating func setDisableMode() { self = .disableMode }

    mutating func setFromIndex(_ index: Int) {
        switch index {
        case 0: self.setAddMode()
        case 1: self.setEditMode()
        case 2: self.setFixMode()
        case 3: self.setDeleteMode()
        case 4: self.setDisableMode()
        default: break
        }
    }
}

public enum EditType: Int {
    case electron = 0
    case slCurrent = 1
    case none = 2

    func isElectron() -> Bool { return self == .electron }
    mutating func setElectronEdit() { self = .electron }

    func isSLCurrent() -> Bool { return self == .slCurrent }
    mutating func setSLCurrentEdit() { self = .slCurrent }

    func isNone() -> Bool { return self == .none }
    mutating func setNone() { self = .none }

    mutating func setFromIndex(_ index: Int) {
        switch index {
        case 0: self.setElectronEdit()
        case 1: self.setSLCurrentEdit()
        case 2: self.setNone()
        default: break
        }
    }
}
