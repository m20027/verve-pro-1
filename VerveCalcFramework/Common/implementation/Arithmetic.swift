//
//  Arithmetic.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/01.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation
import Accelerate

// ans <- x @ y
@inline(__always) public func setAdd(_ ans: inout [Double], x: [Double], y: [Double]) {
    vDSP_vaddD(x, 1, y, 1, &ans, 1, UInt(x.count))
}
@inline(__always) public func setSub(_ ans: inout [Double], x: [Double], y: [Double]) {
    vDSP_vsubD(y, 1, x, 1, &ans, 1, UInt(x.count))
}
@inline(__always) public func setMul(_ ans: inout [Double], x: [Double], y: [Double]) {
    vDSP_vmulD(x, 1, y, 1, &ans, 1, UInt(x.count))
}
@inline(__always) public func setDiv(_ ans: inout [Double], x: [Double], y: [Double]) {
    vDSP_vdivD(y, 1, x, 1, &ans, 1, UInt(x.count))
}

// ans <- x @ const
@inline(__always) public func setAdd(_ ans: inout [Double], x: [Double], const: Double) {
    var lconst = const
    vDSP_vsaddD(x, 1, &lconst, &ans, 1, UInt(x.count))
}
// setSub:ans:x:const は要らない
@inline(__always) public func setMul(_ ans: inout [Double], x: [Double], const: Double) {
    var lconst = const
    vDSP_vsmulD(x, 1, &lconst, &ans, 1, UInt(x.count))
}
@inline(__always) public func setDiv(_ ans: inout [Double], x: [Double], const: Double) {
    var lconst = const
    vDSP_vsdivD(x, 1, &lconst, &ans, 1, UInt(x.count))
}
@inline(__always) public func setDiv(_ ans: inout [Double], const: Double, x: [Double]) {
    var lconst = const
    vDSP_svdivD(&lconst, x, 1, &ans, 1, UInt(x.count))
}

// return x @ y
@inline(__always) public func addX(_ x: [Double], y: [Double]) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    setAdd(&ans, x: x, y: y)
    return ans
}
@inline(__always) public func subX(_ x: [Double], y: [Double]) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    setSub(&ans, x: x, y: y)
    return ans
}
@inline(__always) public func mulX(_ x: [Double], by y: [Double]) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    setMul(&ans, x: x, y: y)
    return ans
}
@inline(__always) public func divX(_ x: [Double], by y: [Double]) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    setDiv(&ans, x: x, y: y)
    return ans
}

// return x @ const
@inline(__always) public func addX(_ x: [Double], const: Double) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    setAdd(&ans, x: x, const: const)
    return ans
}
// subX(x:const) は要らない
@inline(__always) public func mulX(_ x: [Double], const: Double) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    setMul(&ans, x: x, const: const)
    return ans
}
// return x / const
@inline(__always) public func divX(_ x: [Double], const: Double) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    setDiv(&ans, x: x, const: const)
    return ans
}

// return const @ x
@inline(__always) public func divConst(_ const: Double, by x: [Double]) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    setDiv(&ans, const: const, x: x)
    return ans
}

// x @= y
@inline(__always) public func addselfX(_ x: inout [Double], y: [Double]) {
    setAdd(&x, x: x, y: y)
}
@inline(__always) public func subSelfX(_ x: inout [Double], y: [Double]) {
    setSub(&x, x: x, y: y)
}
@inline(__always) public func mulselfX(_ x: inout [Double], y: [Double]) {
    setMul(&x, x: x, y: y)
}
@inline(__always) public func divselfX(_ x: inout [Double], y: [Double]) {
    setDiv(&x, x: x, y: y)
}

// x @= const
public func addselfX(_ x: inout [Double], const: Double) {
    setAdd(&x, x: x, const: const)
}
// subselfX(x:const) は要らない
@inline(__always) public func mulselfX(_ x: inout [Double], const: Double) {
    setMul(&x, x: x, const: const)
}
@inline(__always) public func divselfX(_ x: inout [Double], const: Double) {
    setDiv(&x, x: x, const: const)
}

// return x @ y
@inline(__always) public func + (lhs: [Double], rhs: [Double]) -> [Double] { return addX(lhs, y: rhs) }
@inline(__always) public func - (lhs: [Double], rhs: [Double]) -> [Double] { return subX(lhs, y: rhs) }
@inline(__always) public func * (lhs: [Double], rhs: [Double]) -> [Double] { return mulX(lhs, by: rhs) }
@inline(__always) public func / (lhs: [Double], rhs: [Double]) -> [Double] { return divX(lhs, by: rhs) }

// return x @ const
@inline(__always) public func + (lhs: [Double], rhs: Double) -> [Double] { return addX(lhs, const: rhs) }
@inline(__always) public func - (lhs: [Double], rhs: Double) -> [Double] { return addX(lhs, const: -rhs) }
@inline(__always) public func * (lhs: [Double], rhs: Double) -> [Double] { return mulX(lhs, const: rhs) }
@inline(__always) public func / (lhs: [Double], rhs: Double) -> [Double] { return divX(lhs, const: rhs) }

// return const @ x
@inline(__always) public func + (lhs: Double, rhs: [Double]) -> [Double] { return addX(rhs, const: lhs) }
@inline(__always) public func - (lhs: Double, rhs: [Double]) -> [Double] { return addX(-rhs, const: lhs) }
@inline(__always) public func * (lhs: Double, rhs: [Double]) -> [Double] { return mulX(rhs, const: lhs) }
@inline(__always) public func / (lhs: Double, rhs: [Double]) -> [Double] { return divConst(lhs, by: rhs) }

// -x
@inline(__always) public prefix func - (hs: [Double]) -> [Double] { return mulX(hs, const: -1.0) }

// x @= y
@inline(__always) public func += (lhs: inout [Double], rhs: [Double]) { addselfX(&lhs, y: rhs) }
@inline(__always) public func -= (lhs: inout [Double], rhs: [Double]) { subSelfX(&lhs, y: rhs) }
@inline(__always) public func *= (lhs: inout [Double], rhs: [Double]) { mulselfX(&lhs, y: rhs) }
@inline(__always) public func /= (lhs: inout [Double], rhs: [Double]) { divselfX(&lhs, y: rhs) }

// x @= const
@inline(__always) public func += (lhs: inout [Double], rhs: Double) { addselfX(&lhs, const: rhs) }
@inline(__always) public func -= (lhs: inout [Double], rhs: Double) { addselfX(&lhs, const: -rhs) }
@inline(__always) public func *= (lhs: inout [Double], rhs: Double) { mulselfX(&lhs, const: rhs) }
@inline(__always) public func /= (lhs: inout [Double], rhs: Double) { divselfX(&lhs, const: rhs) }

@inline(__always) public func fill(_ x: inout [Double], const: Double) {
    var lconst = const
    vDSP_vfillD(&lconst, &x, 1, UInt(x.count))
}

precedencegroup FillPrecedence {
    associativity: right
    lowerThan: AdditionPrecedence
    //    precedence: 90
}

infix operator <-: FillPrecedence

@inline(__always) public func <- (lhs: inout [Double], rhs: Double) {
    fill(&lhs, const: rhs)
}

@inline(__always) public func distX(_ x: [Double], y: [Double]) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    vDSP_vdistD(x, 1, y, 1, &ans, 1, UInt(x.count))
    return ans
}

@inline(__always) public func setDist(_ ans: inout [Double], x: [Double], y: [Double]) {
    vDSP_vdistD(x, 1, y, 1, &ans, 1, UInt(x.count))
}

@inline(__always) public func squaringX(_ x: [Double]) -> [Double] {
    var ans = [Double](repeating: 0, count: x.count)
    vDSP_vsqD(x, 1, &ans, 1, UInt(x.count))
    return ans
}

@inline(__always) public func sum(_ x: [Double]) -> Double {
    var ans = 0.0
    vDSP_sveD(x, 1, &ans, UInt(x.count))
    return ans
}

postfix operator **

@inline(__always) public postfix func ** (lhs: [Double]) -> [Double] {
    return squaringX(lhs)
}

public func removeInf(_ x: inout [Double], ref: [Double]) {
    ref.enumerated().forEach { (i, v) in
        if v == 0.0 {
            x[i] = 0.0
        }
    }
}

@inline(__always) public func maximum(_ x: [Double]) -> Double {
    var ans = 0.0
    vDSP_maxvD(x, 1, &ans, UInt(x.count))
    return ans
}

@inline(__always) public func minimum(_ x: [Double]) -> Double {
    var ans = 0.0
    vDSP_minvD(x, 1, &ans, UInt(x.count))
    return ans
}

@inline(__always) public func clipself(_ x: inout [Double], min: Double, max: Double) {
    var lmin = min
    var lmax = max
    vDSP_vclipD(x, 1, &lmin, &lmax, &x, 1, UInt(x.count))
}

@inline(__always) public func int8(_ x: [Double], to: inout [Int8]) {
    vDSP_vfix8D(x, 1, &to, 1, UInt(x.count))
}

@inline(__always) public func abs(_ x: [Double], to: inout [Double]) {
    vDSP_vabsD(x, 1, &to, 1, UInt(x.count))
}

@inline(__always) public func sign(_ x: [Double], to: inout [Double]) {
    var zero = 0.0
    var one = 1.0
    vDSP_vlimD(x, 1, &zero, &one, &to, 1, UInt(x.count))
}

@inline(__always) public func setVsma(_ x: [Double], const: Double,
                                      add: [Double], to: inout [Double]) {
    var lconst = const
    vDSP_vsmaD(x, 1, &lconst, add, 1, &to, 1, UInt(x.count))
}

@inline(__always) public func setMmov(_ x: UnsafePointer<Double>,
                                      offset: Int, to: inout [Double],
                                      m: Int, n: Int, ta: Int, tc: Int) {
    vDSP_mmovD(x.advanced(by: offset), &to, UInt(m), UInt(n), UInt(ta), UInt(tc))
}

@inline(__always) public func nsfil(_ x: [Double], nr: Int, nc: Int, fil: [Double], out: inout [Double]) {
    vDSP_f3x3D(x, UInt(nr), UInt(nc), fil, &out)
}
