//
//  Point2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/01.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public struct Point2D {
    // owned instance (private set)
    public private(set) var x: Double
    public private(set) var y: Double
    
    // computed property
    public var r: Double { return sqrt(x * x + y * y) }
    public var description: String { return "\(x), \(y)" }
    public var angle: Double { return atan2(y, x) }
    
    public init(x: Double = 0.0, y: Double = 0.0) {
        self.x = x
        self.y = y
    }
    
    public init(cgpoint: CGPoint) {
        self.x = Double(cgpoint.x)
        self.y = Double(cgpoint.y)
    }
    
    @inline(__always) public mutating func setValueX(_ x: Double = 0.0, y: Double = 0.0) {
        self.x = x
        self.y = y
    }
    
    @inline(__always) public mutating func addValueX(_ x: Double = 0.0, y: Double = 0.0) {
        self.x += x
        self.y += y
    }
    
    @inline(__always) public mutating func addself(_ other: Point2D) {
        self.x += other.x
        self.y += other.y
    }
    
    @inline(__always) public func add(_ other: Point2D) -> Point2D {
        return Point2D(x: x + other.x, y: y + other.y)
    }
    
    @inline(__always) public mutating func addOther(_ other: Point2D, multiply: Double = 1.0) {
        self.x += multiply * other.x
        self.y += multiply * other.y
    }
    
    @inline(__always) public func diff(_ other: Point2D) -> Point2D {
        return Point2D(x: x - other.x, y: y - other.y)
    }
}

@inline(__always) public func += (lhs: inout Point2D, rhs: Point2D) {
    lhs.addself(rhs)
}

@inline(__always) public func + (lhs: Point2D, rhs: Point2D) -> Point2D {
    return lhs.add(rhs)
}
