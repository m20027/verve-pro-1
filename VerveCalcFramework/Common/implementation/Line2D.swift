//
//  Line2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public class Line2D: NSObject, Sequence {
    // owned instance
    public var theta: Double
    public var used: Bool
    public var reachElectron: Bool
    public var fixedTheta: Double
    
    // owned instance (private set)
    public private(set) var points: [ELDouble2D]
    public private(set) var usablePoint: Int = 1
    //
    // public constant
    public let nPlot: Int
    public let index: Int
    
    // computed property
    open var firstPoint: ELDouble2D! { return points.first }
    
    /**
     nPlotだけ座標を初期化する．ただし先頭座標は論文の記述通りに初期化され，有効座標カウントは1となる．
     - parameter nPlot:保持できる座標の数
     - parameter theta:円形点電荷においてこの電気力線はどの角度から伸びるか
     - parameter x,y:点電荷の座標
     - parameter index:この電気力線の番号（default is 0）
     */
    public init(nPlot: Int, theta: Double, position: Point2D, index: Int = 0) {
        self.nPlot = nPlot > 0 ? nPlot : 1
        points = (0...self.nPlot).map { _ in ELDouble2D() }
        self.theta = theta
        self.index = index
        self.used = false
        self.reachElectron = false
        self.fixedTheta = Double.nan
        super.init()
        clear(position)
    }
    
    open func clear(_ position: Point2D, rotateStartAngle: Double = 0) {
        points[0].point = Point2D(x: cos(theta + rotateStartAngle), y: sin(theta + rotateStartAngle)) + position
        usablePoint = 1
    }

    open func makeIterator() -> AnyIterator<ELDouble2D> {
        var index : Int = 0
        return AnyIterator {
            if index < self.usablePoint {
                let point = self.points[index]
                index += 1
                return point
            } else {
                return .none
            }
        }
    }
    
    /**
     座標を追加する．
     ただしLineが保持できる座標の数を超えている場合，処理は行われない．
     */
    public func addPoint(_ p: Point2D) {
        if(usablePoint < nPlot){
            points[usablePoint].point = p
            points[usablePoint].index = usablePoint
            usablePoint += 1
        }
    }

    public func arrowAngle(_ p: ELDouble2D) -> Double? {
        if p.index < usablePoint-1 {
            let lp = points[p.index + 1]
            return atan2(lp.y - p.y, lp.x - p.x)
        } else {
            return nil
        }
    }

}
