//
//  Print3D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/03.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public struct Point3D {
    // owned instance (private set)
    public private(set) var x: Double
    public private(set) var y: Double
    public private(set) var z: Double

    // computed property
    public var r: Double { return sqrt(x * x + y * y + z * z) }
    public var description : String { return "\( x ), \( y ), \( z )" }
    public var isZero: Bool { return x == 0.0 && y == 0.0 && z == 0.0 }
    
    public init(x: Double = 0.0, y: Double = 0.0, z: Double = 0.0) {
        self.x = x
        self.y = y
        self.z = z
    }
    
    @inline(__always) public mutating func setValueX(_ x:Double=0.0, y:Double=0.0, z:Double=0.0) {
        self.x = x
        self.y = y
        self.z = z
    }
    
    @inline(__always) public mutating func addValueX(_ x:Double=0.0, y:Double=0.0, z:Double=0.0) {
        self.x += x
        self.y += y
        self.z += z
    }
    
    public func isEqual(_ anObject: AnyObject?) -> Bool {
        if let object = anObject as? Point3D {
            return x == object.x && y == object.y && z == object.z
        } else {
            return false
        }
    }
}
