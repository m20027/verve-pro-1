//
//  SLCurrent2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/03.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public class SLCurrent2D: NSObject {
    // owned instance
    open var position : Point2D
    
    // owned instance (private set)
    open private(set) var currentSign : Double
    
    // owned instance (didSet)
    open var current : Double {
        didSet {
            currentSign = current > 0.0 ? 1.0 : -1.0
        }
    }
    
    // computed property
    open var x: Double { return position.x }
    open var y: Double { return position.y }
    
    public init(current: Double, position: Point2D){
        self.position = position
        self.current = current
        self.currentSign = current > 0.0 ? 1.0 : -1.0
    }
    
//    //    public func changeCurrent(current : Double){
//    //        self.current = current
//    //        self.currentSign = current > 0.0 ? 1.0 : -1.0
//    //    }
//    //
//    //    public func changeELDouble(x:Double, y:Double){
//    //        self.position.x = x
//    //        self.position.y = y
//    //    }
//    //
//    //    public func getCurrnet() -> Double{
//    //        return self.current
//    //    }
//    //
//    //    public func getELDouble() -> ELDouble2D{
//    //        return self.position
//    //    }
//    //
//    //    public func getCurrentSign() -> Double {
//    //        return self.currentSign
//    //    }
//    //
//    //    override public func isEqual(object: AnyObject?) -> Bool {
//    //        if let object = object as? SLCurrent2D {
//    //            return (
//    //                self.position.x == object.getELDouble().x &&
//    //                    self.position.y == object.getELDouble().y &&
//    //                    self.getCurrnet() == object.getCurrnet()
//    //            )
//    //        } else {
//    //            return false
//    //        }
//    //    }

}
