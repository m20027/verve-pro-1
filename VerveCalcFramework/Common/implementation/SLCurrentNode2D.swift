//
//  SLCurrentNode2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import SpriteKit

public class SLCurrentNode2D: SKShapeNode {
    // owned instance (private set)
    open private(set) var slCurrent: SLCurrent2D?

    // owned instance (didSet)
    override open var position : CGPoint {
        didSet {
            slCurrent!.position = Point2D(cgpoint: position)
        }
    }

    //  owned instance(private)
    private var selected: Bool = false
    
    public override init(){
        super.init()
    }

    public convenience init(slCurrent: SLCurrent2D){
        self.init()
        self.slCurrent = slCurrent
        setNodeRadiusAndColor(slCurrent.current)
        self.position = CGPoint(x: CGFloat(slCurrent.position.x), y: CGFloat(slCurrent.position.y))
        self.name = "SLCurrentNode2D"
        self.zPosition = 1
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setSelected() {
        selected = true
        setNodeRadiusAndColor(slCurrent!.current)
    }
    
    public func unsetSelected() {
        selected = false
        setNodeRadiusAndColor(slCurrent!.current)
    }

    open func setNodeRadiusAndColor(_ currentValue: Double) {
        slCurrent?.current = currentValue
        let absCurrentValue = abs(CGFloat(currentValue))
        let radius = 25 * (absCurrentValue / 20.0 + 0.5)
        let color = SKColor.white // currentValue<0 ?
        //SKColor(red: 0, green: 0.2, blue: absCurrentValue*3.0/100.0+0.7, alpha: 1) :
        //SKColor(red: absCurrentValue*3.0/100.0+0.7, green: 0.2, blue: 0, alpha: 1)
        let path = CGMutablePath()
        path.move(to: CGPoint(x: radius, y: 0))
        path.addArc(center: CGPoint(x: 0, y: 0), radius: radius, startAngle: 0.0, endAngle: CGFloat(2.0 * Double.pi), clockwise: false)

        if currentValue < 0 {
            let l = radius * CGFloat(cos(Double.pi/4))
            path.move(to: CGPoint(x: l, y: l))
            path.addLine(to: CGPoint(x: -l, y: -l))
            path.move(to: CGPoint(x: l, y: -l))
            path.addLine(to: CGPoint(x: -l, y: l))

            //            CGPathMoveToPoint(path, nil, l, l)
            //            CGPathAddLineToPoint(path, nil, -l, -l)
            //            CGPathMoveToPoint(path, nil, l, -l)
            //            CGPathAddLineToPoint(path, nil, -l, l)
        }else {
            path.move(to: CGPoint(x: radius * 0.2, y: 0))
            path.addArc(center: CGPoint(x: 0, y: 0), radius: radius * 0.2, startAngle: 0.0, endAngle: CGFloat(2.0 * Double.pi), clockwise: false)
            //            CGPathMoveToPoint(path, nil, radius * 0.2, 0)
            //            CGPathAddArc(path, nil, 0, 0, radius * 0.2, 0.0, CGFloat(2.0 * M_PI), false)
        }
        self.path = path
        self.fillColor = color
        self.strokeColor = SKColor(red: 0.722, green: 0.353, blue: 0.973, alpha: 1.0)
        self.lineWidth = selected ? 4 : 2
        self.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.isDynamic = false

    }
//    //
//    //    public func changeSLCurrentCharge(value: Double) {
//    //        self.setNodeRadiusAndColor(currentValue: value)
//    //    }

}
