//
//  Electron2DTest.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import VerveCalc
import XCTest

class Electron2DTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testElectron2Dが初期化できること() {
        //座標のテスト
        [Point2D(x: 0.0, y: 0.0),
         Point2D(x: 1.0, y: -1.0),
         Point2D(x: -10, y: 10.0)].forEach { (p) in
            let e = Electron2D(charge: 0, position: p, division: 0, nPlot: 0)
            XCTAssertEqual(e.position.description, p.description)
        }

        //電荷量のテスト
        let cs = [-10.0, 0.0, 10.0]
        cs.forEach { (c) in
            let e = Electron2D(charge: c, position: Point2D(), division: 0, nPlot: 0)
            XCTAssertEqual(e.charge, c)
        }

        //保持する力線数のテスト
        var args = [1, 10, 0, -10]
        var correct = [1, 10, 1, 1]
        zip(args, correct).forEach { (a, c) in
            let e = Electron2D(charge: 1.0, position: Point2D(), division: a, nPlot: 100)
            XCTAssertEqual(e.nLine, c)
        }

        //保持した力線の先頭座標のテスト
        let testPxyData : [Point2D] = [
            Point2D(x: 2.0, y: 1.0),
            Point2D(x: 1.0, y: 2.0),
            Point2D(x: 0.0, y: 1.0),
            Point2D(x: 1.0, y: 0.0)
        ]
        let e = Electron2D(charge: 1.0, position: Point2D(x: 1.0, y: 1.0), division: testPxyData.count, nPlot: 1)
        zip(e, testPxyData).forEach { (l, d) in
            l.forEach { (p) in
                XCTAssertEqual(p.x, d.x, accuracy: 1e-9)
                XCTAssertEqual(p.y, d.y, accuracy: 1e-9)
            }
        }

        //保持する力線本数のテスト
        args = [100, 0, -1]
        correct = [100, 1, 1]
        zip(args, correct).forEach { (a, c) in
            let e = Electron2D(charge: 1.0, position: Point2D(), division: 10, nPlot: a)
            e.forEach { (l) in
                XCTAssertEqual(l.nPlot, c)
            }
        }
    }
    
    func testChargeがGetterSetterプロパティに対応すること() {
        let e = Electron2D(charge: 1.0, position: Point2D(), division: 2, nPlot: 10)
        e.charge = -2.0
        XCTAssertEqual(e.charge, -2.0)
        XCTAssertEqual(e.chargeSign, -1.0)
        XCTAssertEqual(e.nLine, 2)

        e.charge = 1.0
        XCTAssertEqual(e.charge, 1.0)
        XCTAssertEqual(e.chargeSign, 1.0)
        XCTAssertEqual(e.nLine, 1)
    }

    func testPositionがGetterSetterプロパティに対応すること() {
        let e = Electron2D(charge: 1.0, position: Point2D(), division: 2, nPlot: 10)
        e.position = Point2D(x:10.0, y:2.0)
        XCTAssertEqual(e.position.description, "10.0, 2.0")

        e.position = Point2D(x: -1.0, y:5.0)
        XCTAssertEqual(e.position.description, "-1.0, 5.0")

        e.position = Point2D(x: 0.0, y:0.0)
        XCTAssertEqual(e.position.description, "0.0, 0.0")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
