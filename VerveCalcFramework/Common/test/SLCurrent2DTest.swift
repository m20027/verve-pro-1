//
//  SLCurrent2DTest.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/03.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import XCTest
import VerveCalc

class SLCurrent2DTest: XCTestCase {
    private var s: SLCurrent2D?

    override func setUp() {
        super.setUp()
        s = SLCurrent2D(current: 1.0, position: Point2D())
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSLCurrent2Dが初期化できること() {
        //座標のテスト
        [
            Point2D(x: 0.0, y: 0.0),
            Point2D(x: 1.0, y: -1.0),
            Point2D(x: -10, y: 10.0)
        ].forEach { (p) in
            s = SLCurrent2D(current:0, position: p)
            XCTAssertEqual(s!.position.description, p.description)
        }
        //電荷量のテスト
        let cs = [-10.0, 0.0, 10.0]
        cs.forEach { (c) in
            s = SLCurrent2D(current: c, position: Point2D())
            XCTAssertEqual(s!.current, c)
        }
    }

    func testChargeがGetterSetterプロパティに対応すること() {
        s!.current = -2.0
        XCTAssertEqual(s!.current, -2.0)
        XCTAssertEqual(s!.currentSign, -1.0)

        s!.current = 1.0
        XCTAssertEqual(s!.current, 1.0)
        XCTAssertEqual(s!.currentSign, 1.0)
    }

    func testPositionがGetterSetterプロパティに対応すること() {
        s!.position = Point2D(x:10.0, y:2.0)
        XCTAssertEqual(s!.position.description, "10.0, 2.0")

        s!.position = Point2D(x: -1.0, y:5.0)
        XCTAssertEqual(s!.position.description, "-1.0, 5.0")

        s!.position = Point2D(x: 0.0, y:0.0)
        XCTAssertEqual(s!.position.description, "0.0, 0.0")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
