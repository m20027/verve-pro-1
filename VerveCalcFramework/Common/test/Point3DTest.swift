//
//  Print3DTest.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/03.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import XCTest
import VerveCalc

class Point3DTest: XCTestCase {
    private var p: Point3D?

    override func setUp() {
        super.setUp()
        self.p = Point3D(x: 999.0, y: 666.0, z: 333.0)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testコンストラクタが様々なパターンの引数に対応できること() {
        XCTAssertEqual(Point3D().description, "0.0, 0.0, 0.0")
        XCTAssertEqual(Point3D(x: 1.0).description, "1.0, 0.0, 0.0")
        XCTAssertEqual(Point3D(y: 2.0).description, "0.0, 2.0, 0.0")
        XCTAssertEqual(Point3D(z: 3.0).description, "0.0, 0.0, 3.0")
        XCTAssertEqual(Point3D(x: 1.0, y: 2.0).description, "1.0, 2.0, 0.0")
        XCTAssertEqual(Point3D(y: 2.0, z: 3.0).description,"0.0, 2.0, 3.0")
        XCTAssertEqual(Point3D(x: 1.0, z: 3.0).description, "1.0, 0.0, 3.0")
        XCTAssertEqual(Point3D(x: 1.0, y: 2.0, z: 3.0).description, "1.0, 2.0, 3.0")
    }
    
    func testSetPoint3Dで座標が変更できること() {
        p!.setValueX(); XCTAssertEqual(p!.description, "0.0, 0.0, 0.0")
        p!.setValueX(1.0); XCTAssertEqual(p!.description, "1.0, 0.0, 0.0")
        p!.setValueX(y: 2.0); XCTAssertEqual(p!.description, "0.0, 2.0, 0.0")
        p!.setValueX(z: 3.0); XCTAssertEqual(p!.description, "0.0, 0.0, 3.0")
        p!.setValueX(1.0, y: 2.0); XCTAssertEqual(p!.description, "1.0, 2.0, 0.0")
        p!.setValueX(y: 2.0, z: 3.0); XCTAssertEqual(p!.description, "0.0, 2.0, 3.0")
        p!.setValueX(1.0, z: 3.0); XCTAssertEqual(p!.description, "1.0, 0.0, 3.0")
        p!.setValueX(1.0, y: 2.0, z: 3.0); XCTAssertEqual(p!.description, "1.0, 2.0, 3.0")
    }
    
    func testXYRのgetterプロパティに対応すること() {
        struct XyzrData {
            let x: Double
            let y: Double
            let z: Double
            let r: Double
        }
        [XyzrData(x: 1.0, y: 2.0, z: 3.0, r: sqrt(14.0)),
         XyzrData(x: 3.0, y: 4.0, z: 5.0, r: sqrt(50.0)),
         XyzrData(x: -6.0, y: 8.0, z: 10.0, r: sqrt(200.0))].forEach { (d) in
            p!.setValueX(d.x, y: d.y, z: d.z)
            XCTAssertEqual(p!.x, d.x)
            XCTAssertEqual(p!.y, d.y)
            XCTAssertEqual(p!.z, d.z)
            XCTAssertEqual(p!.r, d.r)
        }
    }
    
    func testAddPoint3Dで座標が変更できること() {
        p!.addValueX(); XCTAssertEqual(p!.description, "999.0, 666.0, 333.0")
        p!.addValueX(1.0); XCTAssertEqual(p!.description, "1000.0, 666.0, 333.0")
        p!.addValueX(y: 2.0); XCTAssertEqual(p!.description, "1000.0, 668.0, 333.0")
        p!.addValueX(z: 3.0); XCTAssertEqual(p!.description, "1000.0, 668.0, 336.0")
        p!.addValueX(1.0, y: 2.0); XCTAssertEqual(p!.description, "1001.0, 670.0, 336.0")
        p!.addValueX(y: 2.0, z: 3.0); XCTAssertEqual(p!.description, "1001.0, 672.0, 339.0")
        p!.addValueX(1.0, z: 3.0); XCTAssertEqual(p!.description, "1002.0, 672.0, 342.0")
        p!.addValueX(1.0, y: 2.0, z: 3.0); XCTAssertEqual(p!.description, "1003.0, 674.0, 345.0")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
