//
//  ArithmeticTest.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/01.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import XCTest
import VerveCalc

class ArithmeticTest: XCTestCase {
    fileprivate var x: [Double]?
    fileprivate var y: [Double]?

    override func setUp() {
        super.setUp()
        self.x = [3.14, 1.59]
        self.y = [2.65, 3.58]
    }
    
    override func tearDown() {
        super.tearDown()
    }

    @inline(__always) func assertEqualsArray(_ lefts: [Double], _ rights: [Double]) {
        zip(lefts, rights).forEach { (a, b) in
            XCTAssertEqual(a, b, accuracy: 1e-9)
        }
    }
    
    func testベクトルの加算_定数加算も含む_ができること() {
        assertEqualsArray(x! + y!, [3.14 + 2.65, 1.59 + 3.58])
        assertEqualsArray(x! + 2.0, [3.14 + 2.0, 1.59 + 2.0])
        assertEqualsArray(10.0 + x!, [3.14 + 10.0, 1.59 + 10.0])
    }
    
    func testベクトル自身への加算ができること() {
        x! += y!
        assertEqualsArray(x!, [3.14 + 2.65, 1.59 + 3.58])
        x! += 10.0
        assertEqualsArray(x!, [3.14 + 2.65 + 10.0, 1.59 + 3.58 + 10.0])
    }

    func testベクトルの乗算_定数乗算も含む_ができること() {
        assertEqualsArray(x! * y!, [3.14 * 2.65, 1.59 * 3.58])
        XCTAssertEqual(x! * 2.0, [3.14 * 2.0, 1.59 * 2.0])
        XCTAssertEqual(10.0 * y!, [2.65 * 10.0, 3.58 * 10.0])
    }

    func testベクトル自身への乗算ができること() {
        x! *= y!
        assertEqualsArray(x!, [3.14 * 2.65, 1.59 * 3.58])
        x! *= 10.0
        assertEqualsArray(x!, [3.14 * 2.65 * 10.0, 1.59 * 3.58 * 10.0])
    }

    func testベクトルの減算_定数減算_定数からの減算も含む_ができること() {
        assertEqualsArray(x! - y!, [3.14 - 2.65, 1.59 - 3.58])
        assertEqualsArray(x! - 2.0, [3.14 - 2.0, 1.59 - 2.0])
        assertEqualsArray(10.0 - x!, [10.0 - 3.14, 10.0 - 1.59])
    }

    func testベクトル自身への減算ができること() {
        x! -= y!
        assertEqualsArray(x!, [3.14 - 2.65, 1.59 - 3.58])
        x! -= 10.0
        assertEqualsArray(x!, [3.14 - 2.65 - 10.0, 1.59 - 3.58 - 10.0])
    }

    func testベクトルの除算_定数除算を含む_ができること() {
        XCTAssertEqual(y! / x!, [2.65 / 3.14, 3.58 / 1.59])
        XCTAssertEqual(x! / 2.0, [3.14 / 2.0, 1.59 / 2.0])
        XCTAssertEqual(10.0 / y!, [10.0 / 2.65, 10.0 / 3.58])
    }

    func testベクトル自身への除算ができること() {
        x! /= y!
        assertEqualsArray(x!, [3.14 / 2.65, 1.59 / 3.58])
        y! /= 10.0
        assertEqualsArray(y!, [2.65 / 10.0, 3.58 / 10.0])
    }

    func testベクトルを一定値で埋められること() {
        x! <- 2.0
        assertEqualsArray(x!, [2.0, 2.0])
        x! <- 5.0
        assertEqualsArray(x!, [5.0, 5.0])
    }

    func testベクトルの距離が計算できること() {
        assertEqualsArray(distX(x!, y: y!), [sqrt(3.14*3.14 + 2.65*2.65), sqrt(1.59*1.59 + 3.58*3.58)])
    }

    func testベクトルの各係数の自乗が計算できること() {
        assertEqualsArray(x!**, [3.14*3.14, 1.59*1.59])
        assertEqualsArray(y!**, [2.65*2.65, 3.58*3.58])
    }

    func testベクトルの総和が計算できること() {
        XCTAssertEqual(sum(x!), 3.14 + 1.59, accuracy: 1e-9)
        XCTAssertEqual(sum(y!), 2.65 + 3.58, accuracy: 1e-9)
    }

    func testInfを0に設定できること() {
        let x = [1.0, 2.0, 0.0]
        var y = 1 / x
        removeInf(&y, ref: x)
        XCTAssertEqual(y, [1.0, 0.5, 0.0])
    }

    func testベクトルの最大値が計算できること() {
        XCTAssertEqual(maximum(x!), 3.14, accuracy: 1e-9)
        XCTAssertEqual(maximum(y!), 3.58, accuracy: 1e-9)
    }

    func testベクトルの最小値が計算できること() {
        XCTAssertEqual(minimum(x!), 1.59, accuracy: 1e-9)
        XCTAssertEqual(minimum(y!), 2.65, accuracy: 1e-9)
    }

    func testベクトルのクリッピングができること() {
        var a = [-2.0, -1.0, 0.0, 0.5, 1.0, 1.5]
        clipself(&a, min: -1.0, max: 1.0)
        assertEqualsArray(a, [-1.0, -1.0, 0.0, 0.5, 1.0, 1.0])
    }

    func testInt8に変換できること() {
        let a = [3.0, 1.0, 4.0, 1.0, 5.0, 9.0]
        var b = [Int8](repeating: 0, count: 6)
        int8(a, to: &b)
        XCTAssertEqual(b, [3, 1, 4, 1, 5, 9].map { Int8($0) })
    }

    func test絶対値が計算できること() {
        let a = [-2.0, -1.0, 0.0, 0.5, 1.0, 1.5]
        var b = [Double](repeating: 0, count: 6)
        abs(a, to: &b)
        assertEqualsArray(b, [2.0, 1.0, 0.0, 0.5, 1.0, 1.5])
    }

    func test符号が計算できること() {
        let a = [-2.0, -1.0, 0.0, 0.5, 1.0, 1.5]
        var b = [Double](repeating: 0, count: 6)
        sign(a, to: &b)
        assertEqualsArray(b, [-1.0, -1.0, 1.0, 1.0, 1.0, 1.0])
    }

    func test定数倍加算が計算できること() {
        let a = [3.0, 1.0, 4.0, 1.0, 5.0, 9.0]
        var b = [2.0, 6.0, 5.0, 3.0, 5.0, 8.0]
        var c = [9.0, 7.0, 9.0, 3.0, 2.0, 3.0]
        let d = 3.0
        setVsma(a, const: d, add: b, to: &c)
        assertEqualsArray(c, [11.0, 9.0, 17.0, 6.0, 20.0, 35.0])
        setVsma(a, const: d, add: b, to: &b)
        assertEqualsArray(b, [11.0, 9.0, 17.0, 6.0, 20.0, 35.0])
    }

    func test行列の部分行列が取り出せること() {
        let a = [
            3.0, 1.0, 4.0, 1.0,
            5.0, 9.0, 2.0, 6.0,
            5.0, 3.0, 5.0, 8.0
        ]
        var b = [Double](repeating: 0.0, count: 6)
        setMmov(a, offset: 5, to: &b, m: 3, n: 2, ta: 4, tc: 3)
        XCTAssertEqual(b, [9.0, 2.0, 6.0, 3.0, 5.0, 8.0])
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
