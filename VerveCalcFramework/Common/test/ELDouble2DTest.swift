//
//  ELDouble2DTest.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import XCTest
import VerveCalc

class ELDouble2DTest: XCTestCase {
    private var p: ELDouble2D?

    override func setUp() {
        super.setUp()
        p = ELDouble2D(x: 999.0, y: 666.0)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testコンストラクタが様々なパターンの引数に対応できること() {
        XCTAssertEqual(ELDouble2D().description, "(0: 0.0, 0.0)")
        XCTAssertEqual(ELDouble2D(x: 1.0).description, "(0: 1.0, 0.0)")
        XCTAssertEqual(ELDouble2D(y: 2.0).description, "(0: 0.0, 2.0)")
        XCTAssertEqual(ELDouble2D(x: 1.0, y: 2.0).description, "(0: 1.0, 2.0)")
        XCTAssertEqual(ELDouble2D(x: 5.0, index: 3).description, "(3: 5.0, 0.0)")
        XCTAssertEqual(ELDouble2D(x: 2.0, y: 3.0, index: 5).description, "(5: 2.0, 3.0)")
    }
    
    func testIndexがGetterSetterプロパティに対応すること() {
        XCTAssertEqual(p!.index, 0)
        p!.index = 1
        XCTAssertEqual(p!.index, 1)
    }

    func testSetELDoubleで座標が変更できること() {
        p!.setValueX(); XCTAssertEqual(p!.description, "(0: 0.0, 0.0)")
        p!.setValueX(1.0); XCTAssertEqual(p!.description, "(0: 1.0, 0.0)")
        p!.setValueX(y: 2.0); XCTAssertEqual(p!.description, "(0: 0.0, 2.0)")
        p!.setValueX(1.0, y: 2.0); XCTAssertEqual(p!.description, "(0: 1.0, 2.0)")
        p!.setValueX(5.0, y: 6.0, index: 3); XCTAssertEqual(p!.description, "(3: 5.0, 6.0)")
    }

    func testXYRのGetterプロパティに対応すること() {
        struct XyrData {
            let x: Double
            let y: Double
            let r: Double
        }
        [
            XyrData(x: 1.0, y: 2.0, r: sqrt(5.0)),
            XyrData(x: 3.0, y: 4.0, r: 5.0),
            XyrData(x: -6.0, y: 8.0, r: 10.0)
            ].forEach { (d) in
                p! = ELDouble2D(x: d.x, y: d.y)
                XCTAssertEqual(p!.x, d.x)
                XCTAssertEqual(p!.y, d.y)
                XCTAssertEqual(p!.r, d.r)
                XCTAssertEqual(p!.angle, atan2(d.y, d.x))
        }
    }

    func testAddValueXで座標が加算できること() {
        p!.addValueX(); XCTAssertEqual(p!.description, "(0: 999.0, 666.0)")
        p!.addValueX(1.0); XCTAssertEqual(p!.description, "(0: 1000.0, 666.0)")
        p!.addValueX(y: 2.0); XCTAssertEqual(p!.description, "(0: 1000.0, 668.0)")
        p!.addValueX(1.0, y: 2.0); XCTAssertEqual(p!.description, "(0: 1001.0, 670.0)")
    }

    func testDiffで差が出力できること() {
        let a = ELDouble2D(x: 1.0, y: 2.0)
        let b = ELDouble2D(x: 3.0, y: 5.0)
        let c = a.diff(b)
        XCTAssertEqual(c.description, "(0: -2.0, -3.0)")
        let d = c.diff(a)
        XCTAssertEqual(d.description, "(0: -3.0, -5.0)")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
